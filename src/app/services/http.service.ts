import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http:HttpClient) { }

  getUserList(){
    return this.http.get(`${environment.hostname}teverto/user`)
  }
  postUser(user:User){
    return this.http.post(`${environment.hostname}teverto/user`,user)
  }
  deleteUser(user:User){
    return this.http.delete(`${environment.hostname}teverto/user?id=${user._id}`)
  }
  patchUser(user:User){
    return this.http.patch(`${environment.hostname}teverto/user?id=${user._id}`,user)
  }
}
