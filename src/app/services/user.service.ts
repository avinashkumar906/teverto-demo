import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor() { }

  userList:Array<User> = new Array<User>()
  userListChange = new BehaviorSubject<Array<User>>([]);
  isUpdate = new Subject<User>()

  putList(userList:Array<User>){
    this.userList = userList;
    this.userListChange.next(this.userList);
  }
  putUser(user:User){
    this.userList.push(user);
    this.userListChange.next(this.userList)
  }
  updateUser(user:User){
    let index = this.userList.findIndex((item:User)=>item._id === user._id)
    if(index >= 0){
      this.userList[index] = user;
      this.userListChange.next(this.userList)
    }
  }
  deleteUser(user:User){
    let index = this.userList.findIndex((item:User)=>item._id === user._id)
    if(index >= 0){
      this.userList.splice(index,1);
      this.userListChange.next(this.userList)
    }
  }


}

export class User{
    _id:string;
    vendorName:string;
    type:string;
    email:string;
    webUrl:string;
    action:string;
}