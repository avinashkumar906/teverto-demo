import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
import { User, UserService } from 'src/app/services/user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit, OnDestroy {

  constructor(
    private httpService:HttpService,
    private userService:UserService
  ) { }

  userList:Array<User> = []
  userSubscription:Subscription;

  ngOnInit(): void {
    this.userSubscription = this.userService.userListChange.subscribe(
      (data:Array<User>)=>this.userList = data,
      (err)=>console.log("Error: ", err)
    )
    this.httpService.getUserList().subscribe(
      (data:Array<User>)=>this.userService.putList(data),
      (err)=>console.log("Error: ", err)
    )
  }

  delete(user){
    this.httpService.deleteUser(user).subscribe(
      (data:User)=>{
        this.userService.deleteUser(user)
      }
    )
  }
  
  update(user){
    this.userService.isUpdate.next(user)
  }

  ngOnDestroy(){
    this.userSubscription.unsubscribe()
  }
}
