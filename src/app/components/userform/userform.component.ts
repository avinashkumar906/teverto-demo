import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpService } from 'src/app/services/http.service';
import { User, UserService } from 'src/app/services/user.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-userform',
  templateUrl: './userform.component.html',
  styleUrls: ['./userform.component.css']
})
export class UserformComponent implements OnInit {

  constructor(
    private formBuilder:FormBuilder,
    private httpService:HttpService,
    private userService:UserService
  ) { }

  userForm:FormGroup;
  isEdit:boolean;
  userSubscription:Subscription

  ngOnInit(): void {
    this.userSubscription = this.userService.isUpdate.subscribe(
      (user:User)=>{
        this.userForm.patchValue(user);
        this.isEdit = true;
      }
    )
    this.userForm = this.formBuilder.group({
      _id:[''],
      vendorName:['',Validators.required],
      type:['',Validators.required],
      email:['',Validators.required],
      webUrl:['',Validators.required],
      action:['',Validators.required],
    })
  }

  addUser(){
    this.httpService.postUser(this.userForm.value).subscribe(
      (data:User)=>{
        this.userService.putUser(data)
      }
    )
  }
  updateUser(){
    this.httpService.patchUser(this.userForm.value).subscribe(
      (data:User)=>{
        this.userService.updateUser(data)
      }
    )
  }
  submit(){
    if(!this.isEdit){
      this.addUser()
    } else{
      this.updateUser()
    }
  }
  reset(){
    this.userForm.reset()
    this.isEdit = false;
  }
  ngOnDestroy(){
    this.userSubscription.unsubscribe()
  }
}
